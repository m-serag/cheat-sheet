# add cron job to delete folders that older than 2 hours

- `crontab -e`
- `0 */2 * * * find /path/to/folder -type d -mmin +120 -exec rm -rf {} \;`