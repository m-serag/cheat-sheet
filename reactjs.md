# Create new app

- `npm i -g create-react-app`
- `npm audit fix`
- `npx create-react-app app-name`

# yarn

### Starts the development server.

- `yarn start`

### Bundles the app into static files for production.

- `yarn build`

# VS Code

- Language mode `javascriptreact`
- Extension `https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets`
	- Use `rafce` to create component in arrow function and export
	- Use `impt` to import PropTypes
	

# npm useful
	- `npm i react-icons`

# Problems
- https://stackoverflow.com/questions/55763428/react-native-error-enospc-system-limit-for-number-of-file-watchers-reached