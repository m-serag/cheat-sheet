storage "consul" { 
    address = "127.0.0.1:8500" 
    path = "vault/"
}
listener "tcp" { 
    address = "0.0.0.0:8200" 
    cluster_address = "0.0.0.0:8201" 
    tls_cert_file = "/etc/certs/vault.crt" 
    tls cert_key ="/etc/certs/vault.key"
}
seal "awskms" { 
    region = "us-east-1" 
    kms_key_id = "b1c5659d-9366-4836-e312-799b9813cbe"
}
api_addr = "https://10.0.0.10:8200" 
ui = true 
cluster_name = "skylines_cluster" 
log_levei = "info"