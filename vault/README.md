## Run
`docker run --name vault_demo -d --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=myroot' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' -e 'VAULT_ADDR=http://0.0.0.0:8200' -p 8200:8200 vault`

## Login
`docker exec vault_demo vault login myroot`

## Web Console
http://localhost:8200/ui/vault/auth?with=token

## Enable version 2
`docker exec vault_demo vault secrets enable -version=2 -path=demo/p1 kv`

- `demo/p1` here is the project path, can be anything but must have at least one `/` so it is not a root point

## Add value
`docker exec vault_demo vault kv put demo/p1 testme1=testme1_value testme2=testme2_value`

- This will override data (not appending to it)

## Course commands
- `vault status`
- `VAULT_ADDR=http://0.0.0.0:8200` is mandatory environment var
- `vault operator init -key-shares=3 -key-threshold=2` to initialize vault using key shares
	- pick up the root init token until we revoke it
	- pick up the unseal keys
- `vault operator unseal` to unseal using one of unseal keys from prev command
- `sudo cat /etc/vault.d/vault.hcl` to show up the configuration file.
- `vault server -dev`
	

## References
- https://github.com/ivangfr/springboot-vault-examples/tree/master/spring-vault-approle-mysql
- https://github.com/ekim197711/springboot-hashicorpvault/blob/master/src/main/java/com/codeinvestigator/vault_demo/configuration/SecretStuffConfig.java
- https://www.youtube.com/watch?v=3uDNaBZ9VLU
- https://learn.hashicorp.com/tutorials/vault/eaas-spring-demo?in=vault/app-integration
- https://github.com/mp911de/spring-cloud-vault-config-samples
- https://learn.hashicorp.com/tutorials/vault/getting-started-intro?in=vault/getting-started