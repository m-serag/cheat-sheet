# Installation
- `sudo apt-get install -y python3-pip python3`
- `curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"`
- `unzip awscliv2.zip`
- `sudo ./aws/install`

# Configure
- `aws configure`
    - AWS Access Key ID [None]: `<access key>`
    - AWS Secret Access Key [None]: `<secret key>`
    - Default region name [None]: `<region>`
    - Default output format [None]: 

# Login

`aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 811242726728.dkr.ecr.us-east-1.amazonaws.com`