## Kill all running processes on database

```
DECLARE @DatabaseName nvarchar(50)
SET @DatabaseName = N'YOUR_DABASE_NAME'

DECLARE @SQL varchar(max)

SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID(@DatabaseName) AND SPId <> @@SPId

--SELECT @SQL 
EXEC(@SQL)

```

## Connect to SQL
```
docker exec -it mssql_2019x /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "Abc@123456"
```

## Create database
```
create database [test-a-rec] COLLATE Arabic_100_CI_AI
```

## Backup
```
BACKUP DATABASE [test-a-standalone] TO DISK = '/var/opt/mssql/backups/test-a-st-s.bak' WITH FORMAT, MEDIANAME = 'SQLServerBackups', NAME = 'Full Backup of test-a-st';
```

## Restore
### Prerequsite
Backup must be compressed and copied into docker container
- `docker exec -it mssql_2019 mkdir /var/opt/mssql/data/bak`
- `docker cp Tenant01.tar.gz mssql_2019:/var/opt/mssql/data/`
- `docker exec -it mssql_2019 tar xvfz /var/opt/mssql/data/Tenant01.tar.gz --directory /var/opt/mssql/data/bak`
### List backup files
```
docker exec -it mssql_2019 /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "Abc@123456" -Q "RESTORE FILELISTONLY FROM DISK = '/var/opt/mssql/data/bak/Tenant01.bak'"
```
### Restore command
```
docker exec -it mssql_2019 /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "Abc@123456" -Q "RESTORE DATABASE [Tenant03] FROM DISK = '/var/opt/mssql/data/bak/Tenant01.bak' WITH MOVE 'OTMS196T1' TO '/var/opt/mssql/data/Tenant03.mdf', MOVE 'OTMS196T1_log' TO '/var/opt/mssql/data/Tenant03.ldf'"
```