- `sed -i '1s/^/append_text/' filename.txt` add `append_text` in the begining of first line

- `sed -i '1,3s/^/append_text/' filename.txt` add `append_text` in the begining of 1st, 2nd and 3rd lines

- `sed -i 's/old_text/new_text/g' filename.txt` change `old_text` to `new_text`

- `echo "last_word" >> filename.txt` add `last_word` in the end of file