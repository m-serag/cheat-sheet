# Commands

## Get command
- `kubectl get all`
- `kubectl get namespaces`
- `kubectl get pods -A`

## Delete command

kubectl delete type/name

`kubectl delete pod/pingpong`

## Create a deployment
`kubectl create deployment nginx --image nginx`

## Create a deployment with command
`kubectl create deployment pingpong --image alpine -- ping 1.1.1.1`

## Attach to pod and open terminal
`kubectl attach -ti pingpong`

## Create a single pod
`kubectl run nginx --image nginx`

## Create a single pod with a custom command
`kubectl run pingpong --image alpine --command -- ping 1.1.1.1`

## Get Logs
- `kubectl logs deployment.apps/pingpong -f`
- `kubectl logs deploy/pingpong -f --tail 10`

## Scale Replica
`kubectl scale deploy/pingpong --replicas 3`

## Stern

To stream logs from different logs in the same time https://github.com/stern/stern

Homebrew needs to be installed first, see Notes.

`stern --tail 10 --timestamps pingpong`

if we faced issue `Error: invalid configuration: no configuration has been provided, try setting KUBERNETES_MASTER environment variable` then run the following to export the config `export KUBECONFIG=/var/snap/microk8s/current/credentials/kubelet.config`

# Notes

## Arch
Deployement
    |_ Replica
        |_ Pod

- when we scale apps, then we ask deployment to scale its replicas

## Homebrew
- Run the following command which derived from here https://brew.sh/

    `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- Then run the commands which printed in the end of the command

    `(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/mohamed/.zprofile`
- Logout and login again