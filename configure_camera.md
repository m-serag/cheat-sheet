## Install Utility
- `sudo apt install v4l-utils`

## List devices
- `v4l2-ctl --list-devices`
```
HD Webcam C615 (usb-0000:00:14.0-1.4):
        /dev/video2
        /dev/video3

Integrated_Webcam_HD: Integrate (usb-0000:00:14.0-5):
        /dev/video0
        /dev/video1

```

## List controls
- `v4l2-ctl -d /dev/video2 --list-ctrls`
```
                     brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=128
                       contrast 0x00980901 (int)    : min=0 max=255 step=1 default=32 value=32
                     saturation 0x00980902 (int)    : min=0 max=255 step=1 default=32 value=32
 white_balance_temperature_auto 0x0098090c (bool)   : default=1 value=1
                           gain 0x00980913 (int)    : min=0 max=255 step=1 default=64 value=105
           power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=1
      white_balance_temperature 0x0098091a (int)    : min=2800 max=6500 step=1 default=4000 value=4204 flags=inactive
                      sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=22 value=22
         backlight_compensation 0x0098091c (int)    : min=0 max=1 step=1 default=1 value=1
                  exposure_auto 0x009a0901 (menu)   : min=0 max=3 default=3 value=3
              exposure_absolute 0x009a0902 (int)    : min=3 max=2047 step=1 default=166 value=600 flags=inactive
         exposure_auto_priority 0x009a0903 (bool)   : default=0 value=1
                   pan_absolute 0x009a0908 (int)    : min=-36000 max=36000 step=3600 default=0 value=0
                  tilt_absolute 0x009a0909 (int)    : min=-36000 max=36000 step=3600 default=0 value=0
                 focus_absolute 0x009a090a (int)    : min=0 max=255 step=17 default=51 value=0 flags=inactive
                     focus_auto 0x009a090c (bool)   : default=1 value=1
                  zoom_absolute 0x009a090d (int)    : min=1 max=5 step=1 default=1 value=1

```

## Get value
- `v4l2-ctl -d /dev/video2 --get-ctrl=power_line_frequency`

## Set value
- `v4l2-ctl -d /dev/video2 --set-ctrl=power_line_frequency=1`

## Getting product and serial
- `udevadm info --attribute-walk --name=/dev/video2`

## Add rule
- `sudo nano /etc/udev/rules.d/98-logitech-camera.rules`
```
# Logitech settings
SUBSYSTEM=="video4linux", KERNEL=="video[0-9]*", ATTRS{product}=="HD Webcam C615", ATTRS{serial}=="A453FA50", RUN="/usr/bin/v4l2-ctl -d $devnode --set-ctrl=power_line_frequency=1"
```

## Resources
- https://www.youtube.com/watch?v=DaZ9zU3tdFY