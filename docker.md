# Installation

## Centos
- `sudo yum install -y yum-utils`
- `sudo yum install docker-ce docker-ce-cli containerd.io` add `--allowearasing` in case it needed

## Ubuntu
- `sudo apt-get update`
- `sudo apt-get install ca-certificates curl gnupg lsb-release`
- `sudo mkdir -m 0755 -p /etc/apt/keyrings`
- `echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`
- `sudo apt-get update`
- `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin`

## Docker compose
- `sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
- `sudo chmod +x /usr/local/bin/docker-compose`

## Docker permissions
- `sudo usermod -aG docker ${USER}`
- `newgrp docker`
- `docker run hello-world`

# Cleaning up

## Remove all containers
- `docker ps -a | grep "user" | awk '{print $1}' | xargs docker rm -f`

## Remove all images
- `docker images -a | grep "user" | awk '{print $3}' | xargs docker rmi -f`

## List all networks
- `docker ps -q | xargs -n 1 docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} {{ .Name }}' | sed 's/ \// /'`

# Build

`docker build -f ~/code/user-management/server/src/main/resources/docker/app/backend/Dockerfile -t usermanagement-app:1.0.0 ~/code/user-management/`

# Push

- `docker tag usermanagement-app:1.0.0 811242726728.dkr.ecr.us-east-1.amazonaws.com/usermanagement-app:latest`
- `docker push 811242726728.dkr.ecr.us-east-1.amazonaws.com/usermanagement-app:latest`