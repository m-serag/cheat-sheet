## Build Project
`yarn install`

## Run Android
`yarn run android`

## Run Packager
this must be after running app - previous command
`yarn start`

## Build Android
- Generate key 
`keytool -genkey -v -keystore my-upload-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000`
Passwords in `android/gradle.properties`

- Move key into `android/app` folder
- `cd androud/`
- `./gradlew assembleRelease`
- `android/app/build/outputs/apk/release/app-release.apk`


