## Docker
- `docker run --name oracledb -d -p 49161:1521 -e ORACLE_ALLOW_REMOTE=true -e ORACLE_PASSWORD=oracle -v oracle-volume:/opt/oracle/oradata gvenzl/oracle-xe`

## Create database
- `create user umtest identified by Abc123456 account unlock;`
- `grant all privileges to umtest;`

## Connect using command line
- `sqlplus`
- *user name* -> `system`
- *password* -> `oracle`

## Sha3tra
- `show user`