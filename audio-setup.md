I was facing the same issue with Oneplus Wireless Z Bass edition. My headset microphone was not detected by PulseAudio and the problem is that my headphones don't have HSP profile, only HFP profile. After trying for 2 days, I came to the solution of replacing PulseAudio with Pipewire sound server, which supports HSP, HFP and A2DP by itself. So there will be no need to install any other utility like ofono, phonesim. Also, to be noted that my problem wasn't resolved even after following all the steps to configure ofono in PulseAudio. So I came up with the steps to replace PulseAudio with PipeWire.

Here is the detailed article I have written to resolve this problem, the steps of which I'm also adding here. You can follow it and most probably be able to solve your problem.

Bluetooth headset microphone not detected

Open your terminal and follow these steps:

We will use a PPA for adding Pipewire to Ubuntu 20.04, which is maintained regularly:

`sudo add-apt-repository ppa:pipewire-debian/pipewire-upstream`

To update the PPA packages in your system do:

`sudo apt update`

Install the package:

`sudo apt install pipewire`

There is also a dependency needed to be installed with Pipewire, otherwise you will face the issue of “Bluetooth headset won’t connect after installing pipewire”. Install the dependency by:

`sudo apt install libspa-0.2-bluetooth`

Now, to install the client libraries:

`sudo apt install pipewire-audio-client-libraries`

Reload the daemon:

`systemctl --user daemon-reload`

Disable PulseAudio:

`systemctl --user --now disable pulseaudio.service pulseaudio.socket`

If you are on Ubuntu 20.04, you also need to “mask” the PulseAudio by:

`systemctl --user mask pulseaudio`

I am not sure but, if possible, you can try to run this on other versions too.
After a new update of Pipewire, you also need to enable pipewire-media-session-service:

`systemctl --user --now enable pipewire-media-session.service`

You can ensure that Pipewire is now running through:

`pactl info`

This command will give the following output, in Server Name you can see:
`PulseAudio (on PipeWire 0.3.28)`

Things should be working by now and you can see your microphone.

If it doesn’t show up, then try restarting Pipewire by this command:

`systemctl --user restart pipewire`

If it’s still not showing your microphone, you can try rebooting once and remove and pair your Bluetooth device again to check if it works now.


**If you want to rollback all the changes we did, you can do it by using:**

`systemctl --user unmask pulseaudio`
`systemctl --user --now enable pulseaudio.service pulseaudio.socket`

https://askubuntu.com/questions/1247209/microphone-not-working-on-bluetooth-earbuds

